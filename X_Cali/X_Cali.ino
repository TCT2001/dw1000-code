#include <AnchorHelper.h>
#define EXPECTED_RANGE 7.94 // Recommended value for default values, refer to chapter 8.3.1 of DW1000 User manual
#define EXPECTED_RANGE_EPSILON 0.05
#define ACCURACY_THRESHOLD 10
#define ANTENNA_DELAY_STEPS 1

byte tag_shortAddress[] = {0x09, 0x00};
int count = 0;
uint32_t t;
boolean done = false;

// Antenna calibration variables
int accuracyCounter = 0;
uint16_t antenna_delay = 16436;

void setup() {
    Serial.begin(115200);
    Serial.println(F("### DW1000Ng-arduino-ranging-anchorMain ###"));

    DW1000Ng::initializeNoInterrupt(10, 9);
    Serial.println(F("DW1000Ng initialized ..."));

    AnchorHelper::generalConfig(MASTER_ANCHOR_FRAME_FILTER_CONFIG);
    DW1000Ng::setEUI("AA:BB:CC:DD:EE:FF:00:01");
    DW1000Ng::setDeviceAddress(1);
    DW1000Ng::setAntennaDelay(16436);

    Serial.println(F("Committed configuration ..."));

    CommonHelper::printConfig();

    // nBitAnchor = 1;
    // activeAnchorValue = 1;
}

void loop() {
    if (CommonHelper::receiveFrame()) {
        size_t recv_len = DW1000Ng::getReceivedDataLength();
        byte recv_data[recv_len];
        DW1000Ng::getReceivedData(recv_data, recv_len);

        if (recv_data[0] == BLINK) {
            AnchorHelper::transmitRangingInitiation(&recv_data[2], tag_shortAddress);
            CommonHelper::waitForTransmission();

            RangeAcceptResult result = AnchorHelper::anchorRangeAccept(NextActivity::ACTIVITY_FINISHED, 0);;
          
            if (!result.success) {
                return;
            }

            CommonHelper::printResult("1", result.range, NULL);

            // Antenna delay script
                if(result.range >= (EXPECTED_RANGE - EXPECTED_RANGE_EPSILON) && result.range <= (EXPECTED_RANGE + EXPECTED_RANGE_EPSILON)) {
                    accuracyCounter++;
                } else {
                    accuracyCounter = 0;
                    antenna_delay += (result.range > EXPECTED_RANGE) ? ANTENNA_DELAY_STEPS : -ANTENNA_DELAY_STEPS;
                    DW1000Ng::setAntennaDelay(antenna_delay);
                }

                if(accuracyCounter == ACCURACY_THRESHOLD) {
                    Serial.print("Found Antenna Delay value (Divide by two if one antenna is set to 0): ");
                    Serial.println(antenna_delay);
                    delay(10000);
                }

            return;
        }
    }
}