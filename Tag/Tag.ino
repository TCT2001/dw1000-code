#include <CommonHelper.h>
#include <TagHelper.h>

// Extended Unique Identifier register. 64-bit device identifier. Register file: 0x01
char EUI[] = "AA:BB:CC:DD:EE:FF:00:09";
uint16_t replyDelay = 650; // Must >= 650 with Range ~ 0.35

void setup() {
    Serial.begin(115200);
    Serial.println(F("### DW1000Ng-arduino-ranging-tag ###"));
    DW1000Ng::initializeNoInterrupt(10, 9);

    TagHelper::generalConfig();
    
    Serial.println("DW1000Ng initialized ...");
    DW1000Ng::setEUI(EUI);
    DW1000Ng::setDeviceAddress(9);
    DW1000Ng::setNetworkId(RTLS_APP_ID);

    CommonHelper::printConfig();
}

void loop() {
    // DW1000Ng::deepSleep();
    // 2, 3 Anchor delay 1000 OKE
    delayMicroseconds(1000);
    // DW1000Ng::spiWakeup();
    // DW1000Ng::setEUI(EUI);
    RangeInfrastructureResult res = TagHelper::tagTwrLocalize(replyDelay);

    // if(res.success)
    //     blink_rate = res.new_blink_rate;
}