#include <AnchorHelper.h>

// Acive Anchor 1; Inactive Anchor 0:
// D C B A
// 0 0 0 0
// Bit: A on , B,C,D off => 1
// Bit: A on, D on, B, C off => 9

const uint8_t selfBitPos = 0;
byte tag_shortAddress[] = {0x05, 0x00};

float ranges[3];

int count = 0;
uint32_t t;
boolean done = false;

void setup() {
    Serial.begin(115200);
    Serial.println(F("### DW1000Ng-arduino-ranging-anchorMain ###"));

    DW1000Ng::initializeNoInterrupt(10, 9);
    Serial.println(F("DW1000Ng initialized ..."));

    AnchorHelper::generalConfig(MASTER_ANCHOR_FRAME_FILTER_CONFIG);
    DW1000Ng::setEUI("AA:BB:CC:DD:EE:FF:00:01");
    DW1000Ng::setDeviceAddress(1);
    DW1000Ng::setAntennaDelay(16436);

    Serial.println(F("Committed configuration ..."));

    CommonHelper::printConfig();

    nBitAnchor = 3;
    activeAnchorValue = 5;
}

void loop() {
    if (CommonHelper::receiveFrame()) {
        size_t recv_len = DW1000Ng::getReceivedDataLength();
        byte recv_data[recv_len];
        DW1000Ng::getReceivedData(recv_data, recv_len);

        if (recv_data[0] == BLINK) {
            AnchorHelper::transmitRangingInitiation(&recv_data[2], tag_shortAddress);
            CommonHelper::waitForTransmission();

            int8_t index = CommonHelper::getNextAnchorBit(selfBitPos, activeAnchorValue, nBitAnchor);

            RangeAcceptResult result;
            if (-1 == index) {
                result = AnchorHelper::anchorRangeAccept(NextActivity::ACTIVITY_FINISHED, 0);
            } else {
                // Serial.println(index + 1);
                // next_anchor == index + 1
                result = AnchorHelper::anchorRangeAccept(NextActivity::RANGING_CONFIRM, index + 1);
            }

            if (!result.success) {
                return;
            }

            ranges[0] = result.range;
            if (nBitAnchor == 1) {
                CommonHelper::printResult("1", result.range, NULL);
            }
            return;
        }

        if (recv_data[9] == 0x60) {
            double range = static_cast<double>(DW1000NgUtils::bytesAsValue(&recv_data[10], 2) / 1000.0);
            for (uint8_t i = 1; i < NUM_ANCHOR; ++i) {
                if (recv_data[7] == allAnchorAddr[i][0] && recv_data[8] == allAnchorAddr[i][1]) {
                    ranges[i] = range;
                    if (round(pow(2, i)) == CommonHelper::setBitNumber(activeAnchorValue)) {
                        for (uint8_t j = 0; j <= i; ++j) {
                            if (-1 != ranges[j]) {
                                CommonHelper::printResult(j, ranges[j], NULL);
                                ranges[j] = -1;
                            }
                        }
                    }

                    return;
                }
            }

            return;
        }
    }
}