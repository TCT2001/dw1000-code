#include <AnchorHelper.h>
#include <t_utils.h>
#define MAX_ANCHOR 3
#define CD 2  // COMMON_DIMEMSIONS =  NUM_ANCHOR - 1
#define FD 2  // FIXED_DIMEMSIONS

// Acive Anchor 1; Inactive Anchor 0:
// D C B A
// 0 0 0 0
// Bit: A on , B,C,D off => 1
// Bit: A on, D on, B, C off => 9

byte tag_shortAddress[] = {0x09, 0x00};

float **b;
float **final_rs;
float **si;
float **temp;

volatile float data[NUM_ANCHOR][3] = {
    {5.2, 0, 0},
    {0, 0, 0},
    {0, 4.8, 0},
    {5.2, 4.8, 0}};

const float si_arr[CD][CD] = {
    {3, 5},
    {2, 8}};

// double ranges[MAX_ANCHOR];
uint16_t a = 0;

void setup() {
    Serial.begin(115200);
    Serial.println(F("### DW1000Ng-arduino-ranging-anchorMain ###"));

    DW1000Ng::initializeNoInterrupt(10, 9);
    // Serial.println(F("DW1000Ng initialized ..."));

    AnchorHelper::generalConfig(MASTER_ANCHOR_FRAME_FILTER_CONFIG);
    DW1000Ng::setEUI("AA:BB:CC:DD:EE:FF:00:01");
    DW1000Ng::setDeviceAddress(1);
    DW1000Ng::setAntennaDelay(16519);

    // Serial.println(F("Committed configuration ..."));

    CommonHelper::printConfig();
    // initWLS();
    initLS();
}

void loop() {
    // if (a == 500) {
    //     return;
    // }

    if (!CommonHelper::receiveFrame()) {
        return;
    }

    size_t recv_len = DW1000Ng::getReceivedDataLength();
    byte recv_data[recv_len];
    DW1000Ng::getReceivedData(recv_data, recv_len);

    if (recv_data[0] == BLINK) {
        AnchorHelper::transmitRangingInitiation(&recv_data[2], tag_shortAddress);
        CommonHelper::waitForTransmission();

        RangeAcceptResult result = AnchorHelper::anchorRangeAccept(NextActivity::RANGING_CONFIRM, 2);

        if (!result.success) {
            return;
        }

        data[0][2] = result.range;
        // CommonHelper::printResult("1", result.range, NULL);
        return;
    }

    if (recv_data[9] == 0x60) {
        double range = static_cast<double>(DW1000NgUtils::bytesAsValue(&recv_data[10], 2) / 1000.0);
        byte addr = recv_data[7];
        // Serial.println(addr);
        Serial.println(allAnchorAddr[4][0]);
        if (addr == allAnchorAddr[1][0]) {
            data[1][2] = range;
        } else if (addr == allAnchorAddr[2][0]) {
            data[2][2] = range;
        } else if (addr == allAnchorAddr[3][0]) {
            data[3][2] = range;
            for (uint8_t j = 0; j < NUM_ANCHOR; ++j) {
                CommonHelper::printResult(j, data[j][2], NULL);
            }
            // a++;
            // calculateWLS();
            // calculateLS();
            // printMatrix(final_rs, FD, 1);
        }
        return;
    }
}

/*************************************************
 * HELPER FUNCTION
 **************************************************/

void initLS() {
    float **a, **at, **temp1, **temp2;
    allocate(&b, FD, 1);
    allocate(&final_rs, FD, 1);
    allocate(&temp, FD, CD);

    allocate(&a, CD, FD);
    allocate(&at, FD, CD);
    allocate(&temp1, FD, FD);
    allocate(&temp2, FD, FD);

    calculatePreLS(a, at, temp1, temp2);

    free2DArray(a, CD);
    free2DArray(at, FD);
    free2DArray(temp1, FD);
    free2DArray(temp2, FD);
}

void initWLS() {
    float **a, **at, **temp1, **temp2, **temp3, **temp4;
    allocate(&b, FD, 1);
    allocate(&final_rs, FD, 1);
    allocate(&temp, FD, CD);
    allocate(&si, CD, CD);

    allocate(&a, CD, FD);
    allocate(&at, FD, CD);
    allocate(&temp1, FD, CD);
    allocate(&temp2, FD, FD);
    allocate(&temp3, FD, FD);
    allocate(&temp4, FD, CD);

    calculatePreWLS(a, at, temp1, temp2, temp3, temp4);

    free2DArray(a, CD);
    free2DArray(at, FD);
    free2DArray(temp1, FD);
    free2DArray(temp2, FD);
    free2DArray(temp3, FD);
    free2DArray(temp4, FD);
}

void calculatePreLS(float **a, float **at, float **temp1, float **temp2) {
    uint8_t i, j;
    // Cal a
    for (i = 1; i <= CD; ++i) {
        for (j = 0; j < FD; ++j) {
            if (j == 0) {
                a[i - 1][j] = data[i][0] - data[0][0];
            } else if (j == 1) {
                a[i - 1][j] = data[i][1] - data[0][1];
            }
        }
    }

    // Cal at
    calculateTransform(at, a, CD, FD);

    // Cal a * at
    multiplyMatrices(at, a, temp1, FD, CD, CD, FD);

    // Cal (a*at)-
    calculateInverse(temp2, temp1, FD, FD);

    // Cal (a*at)- * at
    multiplyMatrices(temp2, at, temp, FD, FD, FD, CD);
}

void calculatePreWLS(float **a, float **at, float **temp1, float **temp2, float **temp3, float **temp4) {
    // Cal a
    for (uint8_t i = 1; i <= CD; ++i) {
        for (uint8_t j = 0; j < FD; ++j) {
            if (j == 0) {
                a[i - 1][j] = data[i][0] - data[0][0];
            } else if (j == 1) {
                a[i - 1][j] = data[i][1] - data[0][1];
            }
        }
    }

    // Cal at
    calculateTransform(at, a, CD, FD);

    // Cal si
    for (uint8_t i = 0; i < CD; ++i)
        for (uint8_t j = 0; j < CD; ++j) {
            si[i][j] = si_arr[i][j];
        }

    multiplyMatrices(at, si, temp1, FD, CD, CD, CD);
    multiplyMatrices(temp1, a, temp2, FD, CD, CD, FD);
    calculateInverse(temp3, temp2, FD, FD);
    multiplyMatrices(temp3, at, temp4, FD, FD, FD, CD);
    multiplyMatrices(temp4, si, temp, FD, CD, CD, CD);
}

void calculateB() {
    float b_temp = data[0][0] * data[0][0] + data[0][1] * data[0][1] - data[0][2] * data[0][2];

    for (uint8_t i = 0; i < CD; ++i) {
        b[i][0] = 0.5 * (data[i + 1][0] * data[i + 1][0] + data[i + 1][1] * data[i + 1][1] - data[i + 1][2] * data[i + 1][2] - b_temp);
    }
}

void calculateLS() {
    calculateB();
    multiplyMatrices(temp, b, final_rs, FD, CD, CD, 1);
}

void calculateWLS() {
    calculateB();
    multiplyMatrices(temp, b, final_rs, FD, CD, CD, 1);
}

void printArr(float *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%f ", a[i]);
    }
    printf("\n");
}