#include <AnchorHelper.h>
#include <CommonHelper.h>

char EUI[] = "AA:BB:CC:DD:EE:FF:00:03";
byte main_anchor_address[] = {0x01, 0x00};
const uint8_t selfBitPos = 1;

void setup() {
    // DEBUG monitoring
    Serial.begin(115200);
    Serial.println(F("### arduino-DW1000Ng-ranging-anchor-C ###"));
    // initialize the driver
    DW1000Ng::initializeNoInterrupt(10, 9);
    Serial.println(F("DW1000Ng initialized ..."));
    // general configuration

    AnchorHelper::generalConfig(SLAVE_ANCHOR_FRAME_FILTER_CONFIG);
    DW1000Ng::setEUI(EUI);
    DW1000Ng::setDeviceAddress(3);
    DW1000Ng::setAntennaDelay(16531);

    Serial.println(F("Committed configuration ..."));
    // DEBUG chip info and registers pretty printed
    CommonHelper::printConfig();
}

void loop() {
    RangeAcceptResult result = AnchorHelper::anchorRangeAccept(NextActivity::RANGING_CONFIRM, 4);
    // RangeAcceptResult result = AnchorHelper::anchorRangeAccept(NextActivity::ACTIVITY_FINISHED, 0);

    if (result.success) {
        // delay(2);  // Tweak based on your hardware
        AnchorHelper::transmitRangeReport(result.range, main_anchor_address);
        // CommonHelper::printResult("2", result.range, NULL);
    }
}