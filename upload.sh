# ls -l /dev/ttyACM*

temp="/dev/ttyACM"
port="${temp}$2"

if [ $1 -eq 3 ]; then
    arduino-cli compile -b arduino:avr:uno X_Cali/ && arduino-cli upload -p $port -b arduino:avr:uno X_Cali/
    exit 1;
fi

if [ $1 -eq 1 ]; then
    arduino-cli compile -b arduino:avr:uno Tag/ && arduino-cli upload -p $port -b arduino:avr:uno Tag/
elif [ $1 -eq 2 ]; then
    if [ "$3" == "A" ]; then
        arduino-cli compile -b arduino:avr:uno AnchorMain/ && arduino-cli upload -p $port -b arduino:avr:uno AnchorMain/

    elif [ "$3" == "B" ]; then
        arduino-cli compile -b arduino:avr:uno AnchorB/ && arduino-cli upload -p $port -b arduino:avr:uno AnchorB/

    else
        arduino-cli compile -b arduino:avr:uno AnchorC/ && arduino-cli upload -p $port -b arduino:avr:uno AnchorC/
    fi
elif [ $1 -eq 11 ]; then
    arduino-cli compile -b arduino:avr:uno AAA/tag/ && arduino-cli upload -p $port -b arduino:avr:uno AAA/tag/
else 
    if [ "$3" == "A" ]; then
        arduino-cli compile -b arduino:avr:uno AAA/main/ && arduino-cli upload -p $port -b arduino:avr:uno AAA/main/
    elif [ "$3" == "B" ]; then
        arduino-cli compile -b arduino:avr:uno AAA/anchor/ && arduino-cli upload -p $port -b arduino:avr:uno AAA/anchor/

    else
        arduino-cli compile -b arduino:avr:uno AnchorC/ && arduino-cli upload -p $port -b arduino:avr:uno AnchorC/
    fi
fi

# arduino-cli compile -b arduino:avr:uno StandardRTLSAnchorB_TWR/ && arduino-cli upload -p /dev/ttyACM2 -b arduino:avr:uno StandardRTLSAnchorB_TWR/
#   arduino-cli compile -b arduino:avr:uno AAA/test/ && arduino-cli upload -p /dev/ttyACM1 -b arduino:avr:uno AAA/test/

