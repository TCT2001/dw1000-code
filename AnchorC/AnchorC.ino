#include <AnchorHelper.h>
#include <CommonHelper.h>

char EUI[] = "AA:BB:CC:DD:EE:FF:00:03";
byte main_anchor_address[] = {0x01, 0x00};
const uint8_t selfBitPos = 2;

void setup() {
    // DEBUG monitoring
    Serial.begin(115200);
    Serial.println(F("### arduino-DW1000Ng-ranging-anchor-C ###"));
    // initialize the driver
    DW1000Ng::initializeNoInterrupt(10, 9);
    Serial.println(F("DW1000Ng initialized ..."));
    // general configuration

    AnchorHelper::generalConfig(SLAVE_ANCHOR_FRAME_FILTER_CONFIG);
    DW1000Ng::setEUI(EUI);
    DW1000Ng::setDeviceAddress(3);
    DW1000Ng::setAntennaDelay(16436);

    Serial.println(F("Committed configuration ..."));
    // DEBUG chip info and registers pretty printed
    CommonHelper::printConfig();

    nBitAnchor = 3;
    activeAnchorValue = 5;
}

void loop() {
    int8_t index = CommonHelper::getNextAnchorBit(selfBitPos, activeAnchorValue, nBitAnchor);
    RangeAcceptResult result;
    if (-1 == index) {
        result = AnchorHelper::anchorRangeAccept(NextActivity::ACTIVITY_FINISHED, 0);
    } else {
        result = AnchorHelper::anchorRangeAccept(NextActivity::RANGING_CONFIRM, index + 1);
    }

    if (result.success) {
        // delay(2);  // Tweak based on your hardware
        AnchorHelper::transmitRangeReport(result.range, main_anchor_address);
        CommonHelper::printResult("3", result.range, NULL);
    }
}
