#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

// g++ -o main main.cpp && ./main

int getNextAnchorBit(int selfBitPos, int activeBitValue, int nBitAnchor) {
    if (selfBitPos >= nBitAnchor) {
        return -1;
    }

    int i = pow(2, selfBitPos + 1);
    int iter = selfBitPos + 1;
    int temp = pow(2, nBitAnchor);

    while (i < temp) {
        if (i & activeBitValue) {
            return iter;
        }
        i <<= 1;
        iter++;
    }

    return -1;
}

static long calculateNewBlinkRate(char frame11, char frame12) {
    long blinkRate = frame11 + static_cast<long>(((frame12 & 0x3F) << 8));
    printf("Next: %ld\n", blinkRate);
    char multiplier = ((frame12 & 0xC0) >> 6);
    if (multiplier == 0x01) {
        blinkRate *= 25;
    } else if (multiplier == 0x02) {
        blinkRate *= 1000;
    }

    return blinkRate;
}

void writeValueToBytes(char data[], long val, long n) {
    for (auto i = 0; i < n; ++i) {
        data[i] = ((val >> (i * 8)) & 0xFF);
    }
}

int setBitNumber(int n) {
    // Below steps set bits after
    // MSB (including MSB)

    // Suppose n is 273 (binary
    // is 100010001). It does following
    // 100010001 | 010001000 = 110011001
    n |= n >> 1;

    // This makes sure 4 bits
    // (From MSB and including MSB)
    // are set. It does following
    // 110011001 | 001100110 = 111111111
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;

    // The naive approach would increment n by 1,
    // so only the MSB+1 bit will be set,
    // So now n theoretically becomes 1000000000.
    // All the would remain is a single bit right shift:
    //    n = n + 1;
    //    return (n >> 1);
    //
    // ... however, this could overflow the type.
    // To avoid overflow, we must retain the value
    // of the bit that could overflow:
    //     n & (1 << ((sizeof(n) * CHAR_BIT)-1))
    // and OR its value with the naive approach:
    //     ((n + 1) >> 1)
    n = ((n + 1) >> 1) |
        (n & (1 << ((sizeof(n) * CHAR_BIT) - 1)));
    return n;
}

int main() {
    // printf("Next: %d\n", getNextAnchorBit(3, 11, 4));
    // long value = 512;
    // char finishValue[2];
    // writeValueToBytes(finishValue, value, 2);
    // calculateNewBlinkRate(finishValue[1], finishValue[2]);

    // printf("Next: %d\t%d\n", finishValue[1], finishValue[2]);
    // printf("Next: %ld\n", calculateNewBlinkRate(finishValue[1], finishValue[2]));

    printf("Next: %d\n", setBitNumber(5));
}